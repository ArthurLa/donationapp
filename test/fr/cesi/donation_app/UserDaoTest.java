package fr.cesi.donation_app;

import java.sql.SQLException;


import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.dao.UserDao;
import fr.cesi.donationapp.dao.impl.UserDaoImpl;
import junit.framework.TestCase;
import junit.framework.TestSuite;

class UserDaoTest extends TestCase {

	private UserDao userDao = null;

	public UserDaoTest(String name) {
        super(name);
        userDao = new UserDaoImpl();
    }

	public void testCreate() {
		User user = null;
		try {
			user = userDao.create(new User("tester", "TesterFirstName", "TesterLasstName", "tester@tester.fr", "1234", true));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotNull(user);
		assertTrue(user.getPseudo().equals("tester"));
		assertTrue(user.getFirstName().equals("TesterFirstName"));
		assertTrue(user.getLastName().equals("TesterLasstName"));
		assertTrue(user.getEmail().equals("tester@tester.fr"));
		assertTrue(user.getPassword().equals("1234"));
		assertTrue(user.isAdmin());
	}

	public void testFindAll() {
		try {
			assertTrue(!userDao.findAll().isEmpty());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testFindOne() {
		User user = null;
		try {	
			user = userDao.create(new User("tester", "TesterFirstName", "TesterLasstName", "tester@tester.fr", "1234", true));
			assertNotNull(userDao.findOne(user.getId()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testDelete() {
		User user = null;
		try {
			user = userDao.findAll().get(0);
			int userId = user.getId();
			userDao.delete(userId);
			assertNull(userDao.findOne(userId));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static junit.framework.Test suite() {
		TestSuite testSuite = new TestSuite();

		System.out.println("Lancement de la suite de test sur UserDao");

		testSuite.addTest(new UserDaoTest("testCreate"));
		testSuite.addTest(new UserDaoTest("testFindAll"));
		testSuite.addTest(new UserDaoTest("testFindOne"));
		testSuite.addTest(new UserDaoTest("testDelete"));
		return testSuite;
	}

}
