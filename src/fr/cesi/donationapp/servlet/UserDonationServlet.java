package fr.cesi.donationapp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.DonationService;
import fr.cesi.donationapp.service.impl.DonationServiceImpl;

/**
 * Servlet implementation class UserDonationServlet
 */
@WebServlet("/user/DonationServlet")
public class UserDonationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DonationService ds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDonationServlet() {
        super();
        ds = new DonationServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		if(request.getSession().getAttribute("user") == null) {
			response.sendRedirect(request.getContextPath() + "/LoginServlet");
		} else {
			System.out.println(ds.getDonationByUser(user.getId()));
			request.getSession().setAttribute("donations", ds.getDonationByUser(user.getId()));
			request.getRequestDispatcher("/WEB-INF/views/my-donations.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
