package fr.cesi.donationapp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import fr.cesi.donationapp.business.Organisation;
import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.OrganisationService;
import fr.cesi.donationapp.service.PotService;
import fr.cesi.donationapp.service.impl.OrganisationServiceImpl;
import fr.cesi.donationapp.service.impl.PotServiceImpl;

/**
 * Servlet implementation class CreatePotServlet
 */
public class CreatePotServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private OrganisationService os;
	private PotService ps;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreatePotServlet() {
        super();
        os = new OrganisationServiceImpl();
        ps = new PotServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("user") == null) {
			response.sendRedirect(request.getContextPath() + "/LoginServlet");
		} else {
			User user = (User) request.getSession().getAttribute("user");
			request.getSession().setAttribute("organisations", os.getOrganisationsByUser(user.getId()));
			request.getRequestDispatcher("/WEB-INF/views/create-pot.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("TITLE");
		String description = request.getParameter("DESCRIPTION");
		Float amount = Float.parseFloat(request.getParameter("AMOUNT"));
		String organisationId = request.getParameter("ORGANISATION");
		Organisation organisation = os.getOrganisationById(Integer.parseInt(organisationId));
		Part part = request.getPart("PICTURE");
		String fileName = part.getSubmittedFileName();
		String img_path =  getServletContext().getInitParameter("img_path");
	    part.write(img_path + fileName);
	    ps.addPot(name, description, amount, organisation, fileName);
	    doGet(request, response);
	}

}
