package fr.cesi.donationapp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.UserService;
import fr.cesi.donationapp.service.impl.UserServiceImpl;

/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserService us;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServlet() {
        super();
        us = new UserServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("FIRST_NAME");
		String lastName = request.getParameter("LAST_NAME");
		String email = request.getParameter("EMAIL");
		String password = request.getParameter("PASSWORD");
		System.out.println(email.split("@")[0]);
		String pseudo = email.split("@")[0];

		User user = us.addUser(pseudo, firstName, lastName, email, password, false);
		if (user == null) {
			request.getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(request, response);
		} else {
			HttpSession session =  request.getSession();
			session.setAttribute("user", user);
			response.sendRedirect("IndexServlet");
		}
	}

}
