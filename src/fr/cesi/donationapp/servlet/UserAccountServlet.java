package fr.cesi.donationapp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.UserService;
import fr.cesi.donationapp.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AccountServlet
 */
@WebServlet("/user/AccountServlet")
public class UserAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserService us;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAccountServlet() {
        super();
        us = new UserServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("user") == null) {
			response.sendRedirect(request.getContextPath() + "/LoginServlet");
		} else {
			request.getRequestDispatcher("/WEB-INF/views/my-account.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");

		String firstName = request.getParameter("FIRST_NAME");
		String lastName = request.getParameter("LAST_NAME");
		String email = request.getParameter("EMAIL");
		String oldPassword = request.getParameter("OLD_PASSWORD");
		String newPassword = request.getParameter("NEW_PASSWORD");
		String confirmPassword = request.getParameter("CONFIRM_PASSWORD");
		System.out.println("Old password = " + oldPassword);
		if (oldPassword != null && oldPassword != "" && newPassword != null && newPassword != "" && confirmPassword != null && confirmPassword != "") {
			System.out.println("OK");
			System.out.println("Password: " + user.getPassword());
			System.out.println("Old Password: " + oldPassword);
			if (user.getPassword() == oldPassword) {
				System.out.println("Modif du mdp");
				user.setPassword(confirmPassword);
				System.out.println("New user password: " + user.getPassword());
				us.updateUser(user);
				doGet(request, response);
			} else {
				System.out.println("bizarre");
			}
			doGet(request, response);
		} else {
			System.out.println("Modif des infos");
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			System.out.println(user);
			us.updateUser(user);
			doGet(request, response);
		}
	}

}
