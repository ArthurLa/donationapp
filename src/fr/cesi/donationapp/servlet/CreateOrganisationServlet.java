package fr.cesi.donationapp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.OrganisationService;
import fr.cesi.donationapp.service.impl.OrganisationServiceImpl;

/**
 * Servlet implementation class CreateOrganisationServlet
 */
public class CreateOrganisationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private OrganisationService os;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateOrganisationServlet() {
        super();
        os = new OrganisationServiceImpl();
    }
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("user") == null) {
			response.sendRedirect(request.getContextPath() + "/LoginServlet");
		} else {
			request.getRequestDispatcher("/WEB-INF/views/create-organisation.jsp").forward(request, response);
		}
		
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		String name = request.getParameter("NAME");
		String description = request.getParameter("DESCRIPTION");
		String siret = request.getParameter("SIRET");
		Part part = request.getPart("PICTURE");
		String fileName = part.getSubmittedFileName();
		String img_path =  getServletContext().getInitParameter("img_path");
	    part.write(img_path + fileName);
	    os.addOrganisation(name, description, siret, fileName, user);
	    doGet(request, response);
	}

}
