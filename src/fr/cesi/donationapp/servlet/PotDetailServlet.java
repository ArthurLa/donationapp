package fr.cesi.donationapp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cesi.donationapp.business.Pot;
import fr.cesi.donationapp.service.DonationService;
import fr.cesi.donationapp.service.PotService;
import fr.cesi.donationapp.service.impl.DonationServiceImpl;
import fr.cesi.donationapp.service.impl.PotServiceImpl;

/**
 * Servlet implementation class PotDetailServlet
 */
@WebServlet("/PotDetailServlet/*")
public class PotDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PotService ps;
	private DonationService ds;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PotDetailServlet() {
        super();
        ps = new PotServiceImpl();
        ds = new DonationServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//int PotId = Integer.parseInt(request.getParameter("PotId"));
		String uri = request.getRequestURI();
		String[] params = uri.split("/");
		int potId = Integer.parseInt(params[params.length-1]);
		Pot pot = ps.getPotById(potId);
		pot.setDonations(ds.getDonationsByPot(pot.getId()));
		request.getSession().setAttribute("pot", pot);
		request.getRequestDispatcher("/WEB-INF/views/pot-detail.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
