package fr.cesi.donationapp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.PotService;
import fr.cesi.donationapp.service.impl.PotServiceImpl;

/**
 * Servlet implementation class UserPotServlet
 */
public class UserPotServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PotService ps;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserPotServlet() {
        super();
        ps = new PotServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("user") == null) {
			response.sendRedirect(request.getContextPath() + "/LoginServlet");
		} else {
			User user = (User) request.getSession().getAttribute("user");
			request.getSession().setAttribute("pots", ps.getPotsByUser(user.getId()));
			request.getRequestDispatcher("/WEB-INF/views/my-pots.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
