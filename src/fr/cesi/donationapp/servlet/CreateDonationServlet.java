package fr.cesi.donationapp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cesi.donationapp.business.Pot;
import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.DonationService;
import fr.cesi.donationapp.service.PotService;
import fr.cesi.donationapp.service.impl.DonationServiceImpl;
import fr.cesi.donationapp.service.impl.PotServiceImpl;

/**
 * Servlet implementation class CreateDonationServlet
 */
@WebServlet("/CreateDonationServlet/*")
public class CreateDonationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PotService ps;
	private DonationService ds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateDonationServlet() {
        super();
        ps = new PotServiceImpl();
        ds = new DonationServiceImpl();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		String uri = request.getRequestURI();
		String[] params = uri.split("/");
		int potId = Integer.parseInt(params[params.length-1]);
		Pot pot = ps.getPotById(potId);
		Float amount = Float.parseFloat(request.getParameter("AMOUNT"));
		ds.addDonation(amount, user, pot);
	}

}
