package fr.cesi.donationapp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.OrganisationService;
import fr.cesi.donationapp.service.impl.OrganisationServiceImpl;

/**
 * Servlet implementation class UserOrganisationServlet
 */
@WebServlet("/user/OrganisationServlet")
public class UserOrganisationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private OrganisationService os;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserOrganisationServlet() {
        super();
        os = new OrganisationServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		if(request.getSession().getAttribute("user") == null) {
			response.sendRedirect(request.getContextPath() + "/LoginServlet");
		} else {
			request.getSession().setAttribute("organisations", os.getOrganisations());
			request.getRequestDispatcher("/WEB-INF/views/my-organisations.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
