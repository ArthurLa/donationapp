package fr.cesi.donationapp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.donationapp.business.Pot;
import fr.cesi.donationapp.dao.DatabaseConnector;
import fr.cesi.donationapp.dao.OrganisationDao;
import fr.cesi.donationapp.dao.PotDao;

public class PotDaoImpl implements PotDao {
	private Connection connection;
	private OrganisationDao organisationDao;
	
	public PotDaoImpl() {
		try {
			connection = DatabaseConnector.getConnection();
			organisationDao = new OrganisationDaoImpl();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	@Override
	public Pot create(Pot pot) throws SQLException {
		System.out.println("Pot: " + pot);
		java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ADD_POT,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, pot.getTitle());
		ps.setString(2, pot.getDescription());
		ps.setFloat(3, pot.getAmountRequested());
		ps.setInt(4, pot.getOwner().getId());
		ps.setString(5, pot.getPicture());

		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		pot.setId(rs.getInt(1));
		return pot;
	}

	@Override
	public Pot update(Pot pot) throws SQLException {
		java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.UPDATE_ORGANISATION,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, pot.getTitle());
		ps.setString(2, pot.getDescription());
		ps.setFloat(3, pot.getAmountRequested());
		ps.setInt(4, pot.getId());
		
		ps.executeUpdate();
		return pot;
	}

	@Override
	public List<Pot> findAll() throws SQLException {
		List<Pot> pots = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(fr.cesi.donationapp.dao.Requests.ALL_THE_POTS);
		while (rs.next()) {
			Pot pot = new Pot();
			pot.setId(rs.getInt(1));
			pot.setTitle(rs.getString(2));
			pot.setDescription(rs.getString(3));
			pot.setAmountRequested(rs.getFloat(4));
			pot.setOwner(organisationDao.findOne(rs.getInt(5)));
			pot.setPicture(rs.getString(6));
			pots.add(pot);
		}
		return pots;
	}
	
	@Override
	public List<Pot> findAllByOrganisation(int id) throws SQLException {
		List<Pot> pots = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ALL_THE_POTS_BY_ORGANISATION);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Pot pot = new Pot();
			pot.setId(rs.getInt(1));
			pot.setTitle(rs.getString(2));
			pot.setDescription(rs.getString(3));
			pot.setAmountRequested(rs.getFloat(4));
			pot.setOwner(organisationDao.findOne(rs.getInt(5)));
			pot.setPicture(rs.getString(6));
			pots.add(pot);
		}
		return pots;
	}
	
	@Override
	public List<Pot> findAllByUser(int id) throws SQLException {
		List<Pot> pots = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ALL_THE_POTS_BY_USER);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Pot pot = new Pot();
			pot.setId(rs.getInt(1));
			pot.setTitle(rs.getString(2));
			pot.setDescription(rs.getString(3));
			pot.setAmountRequested(rs.getFloat(4));
			pot.setOwner(organisationDao.findOne(rs.getInt(5)));
			pot.setPicture(rs.getString(6));
			pots.add(pot);
		}
		return pots;
	}

	@Override
	public Pot findOne(int id) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.POT_BY_ID);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Pot pot = new Pot();
			pot.setId(rs.getInt(1));
			pot.setTitle(rs.getString(2));
			pot.setDescription(rs.getString(3));
			pot.setAmountRequested(rs.getFloat(4));
			pot.setOwner(organisationDao.findOne(rs.getInt(5)));
			pot.setPicture(rs.getString(6));
			return pot;
		}
		return null;
	}

	@Override
	public boolean delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
