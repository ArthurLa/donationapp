package fr.cesi.donationapp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.donationapp.business.Donation;
import fr.cesi.donationapp.dao.DatabaseConnector;
import fr.cesi.donationapp.dao.UserDao;
import fr.cesi.donationapp.dao.DonationDao;
import fr.cesi.donationapp.dao.PotDao;

public class DonationDaoImpl implements DonationDao {
	private Connection connection;
	private UserDao userDao;
	private PotDao potDao;
	
	public DonationDaoImpl() {
		try {
			connection = DatabaseConnector.getConnection();
			userDao = new UserDaoImpl();
			potDao = new PotDaoImpl();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Override
	public Donation create(Donation donation) throws SQLException {
		java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ADD_DONATION,
				Statement.RETURN_GENERATED_KEYS);
		java.sql.Timestamp sqlDate = new java.sql.Timestamp(donation.getDate().getTime());
		ps.setFloat(1, donation.getAmount());
		ps.setTimestamp(2, sqlDate);
		ps.setInt(3, donation.getRecipient().getId());
		ps.setInt(4, donation.getPot().getId());

		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		donation.setId(rs.getInt(1));
		return donation;
	}

	@Override
	public Donation update(Donation donation) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Donation> findAll() throws SQLException {
		List<Donation> donations = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(fr.cesi.donationapp.dao.Requests.ALL_THE_DONATIONS);
		while (rs.next()) {
			Donation donation = new Donation();
			donation.setId(rs.getInt(1));
			donation.setAmount(rs.getFloat(2));
			donation.setDate(rs.getTimestamp(3));
			donation.setRecipient(userDao.findOne(rs.getInt(4)));
			donation.setPot(potDao.findOne(rs.getInt(5)));
			donations.add(donation);
		}
		return donations;
	}
	
	@Override
	public List<Donation> findAllByPot(int id) throws SQLException {
		List<Donation> donations = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ALL_THE_DONATIONS_BY_POT);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Donation donation = new Donation();
			donation.setId(rs.getInt(1));
			donation.setAmount(rs.getFloat(2));
			donation.setDate(rs.getTimestamp(3));
			donation.setRecipient(userDao.findOne(rs.getInt(4)));
			donation.setPot(potDao.findOne(rs.getInt(5)));
			donations.add(donation);
		}
		return donations;
	}
	
	@Override
	public List<Donation> findAllByUser(int id) throws SQLException {
		List<Donation> donations = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ALL_THE_DONATIONS_BY_USER);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Donation donation = new Donation();
			donation.setId(rs.getInt(1));
			donation.setAmount(rs.getFloat(2));
			donation.setDate(rs.getTimestamp(3));
			donation.setRecipient(userDao.findOne(rs.getInt(4)));
			donation.setPot(potDao.findOne(rs.getInt(5)));
			donations.add(donation);
		}
		System.out.println("Result: " + donations);
		return donations;
	}

	@Override
	public Donation findOne(int id) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.DONATION_BY_ID);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Donation donation = new Donation();
			donation.setId(rs.getInt(1));
			donation.setAmount(rs.getFloat(2));
			donation.setDate(rs.getTimestamp(3));
			donation.setRecipient(userDao.findOne(rs.getInt(4)));
			return donation;
		}
		return null;
	}

	@Override
	public boolean delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
