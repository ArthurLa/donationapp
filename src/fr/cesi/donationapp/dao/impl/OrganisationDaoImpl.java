package fr.cesi.donationapp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.donationapp.business.Organisation;
import fr.cesi.donationapp.dao.DatabaseConnector;
import fr.cesi.donationapp.dao.OrganisationDao;
import fr.cesi.donationapp.dao.UserDao;

public class OrganisationDaoImpl implements OrganisationDao {
	private Connection connection;
	private UserDao userDao;
	
	public OrganisationDaoImpl() {
		try {
			connection = DatabaseConnector.getConnection();
			userDao = new UserDaoImpl();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Override
	public Organisation create(Organisation organisation) throws SQLException {
		java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ADD_ORGANISATION,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, organisation.getName());
		ps.setString(2, organisation.getDescription());
		ps.setString(3, organisation.getSiret());
		ps.setString(4, organisation.getPicture());
		ps.setInt(5, organisation.getManager().getId());

		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		organisation.setId(rs.getInt(1));
		return organisation;
	}

	@Override
	public Organisation update(Organisation organisation) throws SQLException {
		java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.UPDATE_ORGANISATION,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, organisation.getName());
		ps.setString(2, organisation.getDescription());
		ps.setString(3, organisation.getSiret());
		ps.setString(4, organisation.getPicture());
		ps.setInt(5, organisation.getId());
		
		ps.executeUpdate();
		return organisation;
	}

	@Override
	public List<Organisation> findAll() throws SQLException {
		List<Organisation> organisations = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(fr.cesi.donationapp.dao.Requests.ALL_THE_ORGANISATIONS);
		while (rs.next()) {
			Organisation organisation = new Organisation();
			organisation.setId(rs.getInt(1));
			organisation.setName(rs.getString(2));
			organisation.setDescription(rs.getString(3));
			organisation.setSiret(rs.getString(4));
			organisation.setPicture(rs.getString(5));
			organisation.setManager(userDao.findOne(rs.getInt(6)));
			organisations.add(organisation);
		}
		return organisations;
	}
	
	@Override
	public List<Organisation> findAllByUser(int id) throws SQLException {
		List<Organisation> organisations = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ALL_THE_ORGANISATIONS_BY_USER);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Organisation organisation = new Organisation();
			organisation.setId(rs.getInt(1));
			organisation.setName(rs.getString(2));
			organisation.setDescription(rs.getString(3));
			organisation.setSiret(rs.getString(4));
			organisation.setPicture(rs.getString(5));
			organisation.setManager(userDao.findOne(rs.getInt(6)));
			organisations.add(organisation);
		}
		return organisations;
	}

	@Override
	public Organisation findOne(int id) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ORGANISATION_BY_ID);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Organisation organisation = new Organisation();
			organisation.setId(rs.getInt(1));
			organisation.setName(rs.getString(2));
			organisation.setDescription(rs.getString(3));
			organisation.setSiret(rs.getString(4));
			organisation.setPicture(rs.getString(5));
			organisation.setManager(userDao.findOne(rs.getInt(6)));
			return organisation;
		}
		return null;
	}

	@Override
	public boolean delete(int id) throws SQLException {
		if (findOne(id)!=null)
		{
			java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.DELETE_ORGANISATION);
			ps.setInt(1, id);
			ps.executeUpdate();
		}
		return false;
	}

}
