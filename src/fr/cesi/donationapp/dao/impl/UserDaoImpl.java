package fr.cesi.donationapp.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.dao.DatabaseConnector;
import fr.cesi.donationapp.dao.UserDao;

public class UserDaoImpl implements UserDao {
	private Connection connection;
	
	public UserDaoImpl() {
		try {
			this.connection = DatabaseConnector.getConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Override
	public User create(User user) throws SQLException {
		java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.ADD_USER,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, user.getPseudo());
		ps.setString(2, user.getFirstName());
		ps.setString(3, user.getLastName());
		ps.setString(4, user.getEmail());
		ps.setString(5, user.getPassword());
		ps.setBoolean(6, user.isAdmin());
		
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		user.setId(rs.getInt(1));
		return user;
	}

	@Override
	public User update(User user) throws SQLException {
		System.out.println("user: " + user);
		java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.UPDATE_USER,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, user.getFirstName());
		ps.setString(2, user.getLastName());
		ps.setString(3, user.getEmail());
		ps.setString(4, user.getPassword());
		ps.setInt(5, user.getId());
		
		ps.executeUpdate();
		return user;
	}

	@Override
	public List<User> findAll() throws SQLException {
		List<User> users = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(fr.cesi.donationapp.dao.Requests.ALL_THE_USERS);
		while (rs.next()) {
			User user = new User();
			user.setId(rs.getInt(1));
			user.setPseudo(rs.getString(2));
			user.setFirstName(rs.getString(3));
			user.setLastName(rs.getString(4));
			user.setEmail(rs.getString(5));
			user.setAdmin(rs.getBoolean(7));
			users.add(user);
		}
		return users;
	}

	@Override
	public User findOne(int id) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.USER_BY_ID);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			User user = new User();
			user.setId(rs.getInt(1));
			user.setPseudo(rs.getString(2));
			user.setFirstName(rs.getString(3));
			user.setLastName(rs.getString(4));
			user.setEmail(rs.getString(5));
			user.setAdmin(rs.getBoolean(7));
			return user;
		}
		return null;
	}

	@Override
	public User findByMailAndPassword(String email, String password) throws SQLException {
		java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.GET_USER,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, email);
		ps.setString(2, password);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			User user = new User();
			user.setId(rs.getInt(1));
			user.setPseudo(rs.getString(2));
			user.setFirstName(rs.getString(3));
			user.setLastName(rs.getString(4));
			user.setEmail(rs.getString(5));
			user.setPassword(rs.getString(6));
			user.setAdmin(rs.getBoolean(7));
			return user;
		}
		return null;
	}

	@Override
	public boolean delete(int id) throws SQLException {
		if (findOne(id)!=null)
		{
			java.sql.PreparedStatement ps = connection.prepareStatement(fr.cesi.donationapp.dao.Requests.DELETE_USER);
			ps.setInt(1, id);
			ps.executeUpdate();
		}
		return false;
	}

}
