package fr.cesi.donationapp.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cesi.donationapp.business.Pot;

public interface PotDao {
	public Pot create(Pot pot) throws SQLException;
	public Pot update(Pot pot) throws SQLException;
	public List<Pot> findAll() throws SQLException;
	public List<Pot> findAllByOrganisation(int id) throws SQLException;
	public List<Pot> findAllByUser(int id) throws SQLException;
	public Pot findOne(int id) throws SQLException;
	public boolean delete(int id) throws SQLException;
}
