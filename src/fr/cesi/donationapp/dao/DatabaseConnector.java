package fr.cesi.donationapp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = null;
        try {
        	System.out.println("Connecting to database...");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/donation_app?verifyServerCertificate=false&useSSL=false","root","root");
            System.out.println("Connected to database!");
            return connection ;
        } catch (Exception e) {
			System.out.println("Connexion echou�e");
			System.out.println(e);
		} 
        return null;
    }
	
	public static void close() throws ClassNotFoundException, SQLException {
		getConnection().close();
	}

}
