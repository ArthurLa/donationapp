package fr.cesi.donationapp.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cesi.donationapp.business.Donation;

public interface DonationDao {
	public Donation create(Donation donation) throws SQLException;
	public Donation update(Donation donation) throws SQLException;
	public List<Donation> findAll() throws SQLException;
	public List<Donation> findAllByPot(int id) throws SQLException;
	public List<Donation> findAllByUser(int id) throws SQLException;
	public Donation findOne(int id) throws SQLException;
	public boolean delete(int id) throws SQLException;
}
