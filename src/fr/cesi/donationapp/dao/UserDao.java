package fr.cesi.donationapp.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cesi.donationapp.business.User;


public interface UserDao {
	public User create(User user) throws SQLException;
	public User update(User user) throws SQLException;
	public List<User> findAll() throws SQLException;
	public User findOne(int id) throws SQLException;
	public User findByMailAndPassword(String email, String password) throws SQLException;
	public boolean delete(int id) throws SQLException ;
}