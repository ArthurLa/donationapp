package fr.cesi.donationapp.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cesi.donationapp.business.Organisation;

public interface OrganisationDao {
	public Organisation create(Organisation organisation) throws SQLException;
	public Organisation update(Organisation organisation) throws SQLException;
	public List<Organisation> findAll() throws SQLException;
	public List<Organisation> findAllByUser(int id) throws SQLException;
	public Organisation findOne(int id) throws SQLException;
	public boolean delete(int id) throws SQLException;
}
