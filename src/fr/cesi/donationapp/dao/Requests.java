package fr.cesi.donationapp.dao;

public class Requests {
	public static final String ALL_THE_USERS = "SELECT * FROM User";
	public static final String ALL_THE_DONATIONS = "SELECT * FROM Donation";
	public static final String ALL_THE_DONATIONS_BY_POT = "SELECT * FROM Donation WHERE pot_id=?";
	public static final String ALL_THE_POTS = "SELECT * FROM Pot";
	public static final String ALL_THE_ORGANISATIONS = "SELECT * FROM Organisation";

	public static final String USER_BY_ID = "SELECT * FROM User WHERE id=?";
	public static final String GET_USER = "SELECT * FROM User WHERE email=? AND password=?";
	public static final String DONATION_BY_ID = "SELECT * FROM Donation WHERE Id=?";
	public static final String POT_BY_ID = "SELECT * FROM Pot WHERE Id=?";
	public static final String ORGANISATION_BY_ID = "SELECT * FROM Organisation WHERE Id=?";
	public static final String ALL_THE_DONATIONS_BY_USER = "SELECT * FROM Donation WHERE user_id=?";
	public static final String ALL_THE_DONATIONS_BY_POTS = "SELECT * FROM Donation WHERE pot_id=?";
	public static final String ALL_THE_POTS_BY_ORGANISATION = "SELECT * FROM Pot WHERE organisation_id=?";
	public static final String ALL_THE_POTS_BY_USER = "SELECT P.id, P.title, P.description, P.amount, P.organisation_id, P.picture FROM Pot AS P, Organisation as O, User as U WHERE P.organisation_id = O.id AND O.user_id = U.id AND U.id = ?";
	public static final String ALL_THE_ORGANISATIONS_BY_USER = "SELECT * FROM Organisation WHERE user_id=?";

	public static final String ADD_USER = "INSERT INTO User (pseudo, first_name, last_name, email, password, is_admin) VALUES (?,?,?,?,?,?)";
	public static final String ADD_DONATION = "INSERT INTO Donation (amount, date, user_id, pot_id) VALUES (?,?,?,?)";
	public static final String ADD_POT = "INSERT INTO Pot (title, description, amount, organisation_id, picture) VALUES (?,?,?,?,?)";
	public static final String ADD_ORGANISATION = "INSERT INTO Organisation (name, description, siret, picture, user_id) VALUES (?,?,?,?,?)";
	
	public static final String UPDATE_USER = "UPDATE User SET first_name=?, last_name=?, email=?, password=? WHERE id=?";
	public static final String UPDATE_POT = "UPDATE Pot SET title=?, description=?, amount=? WHERE id=?";
	public static final String UPDATE_ORGANISATION = "UPDATE Organisation SET name=?, description=?, siret=?, picture=? WHERE id=?";
	
	public static final String DELETE_USER = "DELETE FROM User WHERE id=?";
	public static final String DELETE_ORGANISATION = "DELETE FROM Organisation WHERE id=?";
}
