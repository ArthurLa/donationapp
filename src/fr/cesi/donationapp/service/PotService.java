package fr.cesi.donationapp.service;

import java.util.List;

import fr.cesi.donationapp.business.Organisation;
import fr.cesi.donationapp.business.Pot;

public interface PotService {
	//c
	public Pot addPot(String name, String description, float amountRequested, Organisation owner, String picture);
		
	//r
	public Pot getPotById(int id);
	public List<Pot> getPots();
	public List<Pot> getPotsByOrganisation(int id);
	public List<Pot> getPotsByUser(int id);
		
	//u	
	public void updatePot(Pot pot);
		
	//d
	public boolean deletePot(int id);
}
