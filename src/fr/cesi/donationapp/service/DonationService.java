package fr.cesi.donationapp.service;

import java.util.List;

import fr.cesi.donationapp.business.Donation;
import fr.cesi.donationapp.business.Pot;
import fr.cesi.donationapp.business.User;

public interface DonationService {
	//c
	public Donation addDonation(float amount, User recipient, Pot pot);
		
	//r
	public Donation getDonationById(int id);
	public List<Donation> getDonations();
	public List<Donation> getDonationsByPot(int id);
	public List<Donation> getDonationByUser(int id);
		
	//u	
	public void updateDonation(Donation donation);
		
	//d
	public boolean deleteDonation(int id);
}
