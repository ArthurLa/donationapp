package fr.cesi.donationapp.service;

import java.util.List;

import fr.cesi.donationapp.business.User;

public interface UserService {
	//c
	public User addUser(String pseudo, String firstName, String lastName, String email, String password, boolean administrator);
		
	//r
	public User getUserById(int id);
	public User getUser(String email, String password);

	public List<User> getUsers();
		
	//u	
	public void updateUser(User user);
		
	//d
	public boolean deleteUser(int id);
}
