package fr.cesi.donationapp.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.cesi.donationapp.business.Organisation;
import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.dao.OrganisationDao;
import fr.cesi.donationapp.dao.impl.OrganisationDaoImpl;
import fr.cesi.donationapp.service.OrganisationService;

public class OrganisationServiceImpl implements OrganisationService {
	private OrganisationDao organisationDao;
	
	public OrganisationServiceImpl() {
		organisationDao = new OrganisationDaoImpl();
	}

	@Override
	public Organisation addOrganisation(String name, String description, String siret, String picture, User user) {
		Organisation organisation = new Organisation(name, description, siret, picture, user);
		try {
			organisation = organisationDao.create(organisation);
			return organisation;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Organisation getOrganisationById(int id) {
		Organisation organisation = null;
		try {
			organisation = organisationDao.findOne(id);
			return organisation;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Organisation> getOrganisations() {
		try {
			return organisationDao.findAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Organisation> getOrganisationsByUser(int id) {
		try {
			return organisationDao.findAllByUser(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void updateOrganisation(Organisation organisation) {
		try {
			organisationDao.update(organisation);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean deleteOrganisation(int id) {
		try {
			organisationDao.delete(id);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

}
