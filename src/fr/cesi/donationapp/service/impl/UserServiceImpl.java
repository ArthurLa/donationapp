package fr.cesi.donationapp.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.dao.UserDao;
import fr.cesi.donationapp.dao.impl.UserDaoImpl;
import fr.cesi.donationapp.service.UserService;

public class UserServiceImpl implements UserService {
	private UserDao utilDao;

	public UserServiceImpl() {
		utilDao = new UserDaoImpl();
	}

	@Override
	public User addUser(String pseudo, String firstName, String lastName, String email, String password,
			boolean administrator) {
		User user = new User(pseudo,firstName, lastName, email, password, administrator);
		System.out.println(user);
		try {
			user = utilDao.create(user);
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public User getUserById(int id) {
		User user = null;
		try {
			user = utilDao.findOne(id);
			return user;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public User getUser(String email, String password) {
		User user = new User(email, password);
		try {
			user = utilDao.findByMailAndPassword(email, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public List<User> getUsers() {
		try {
			return utilDao.findAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void updateUser(User user) {
		try {
			utilDao.update(user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean deleteUser(int id) {
		try {
			utilDao.delete(id);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

}
