package fr.cesi.donationapp.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.cesi.donationapp.business.Donation;
import fr.cesi.donationapp.business.Pot;
import fr.cesi.donationapp.business.User;
import fr.cesi.donationapp.service.DonationService;
import fr.cesi.donationapp.dao.DonationDao;
import fr.cesi.donationapp.dao.impl.DonationDaoImpl;

public class DonationServiceImpl implements DonationService {
	private DonationDao donationDao;
	
	public DonationServiceImpl() {
		donationDao = new DonationDaoImpl();
	}

	@Override
	public Donation addDonation(float amount, User recipient, Pot pot) {
		Donation donation = new Donation(amount, recipient, pot);
		try {
			donation = donationDao.create(donation);
			return donation;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Donation getDonationById(int id) {
		Donation donation = null;
		try {
			donation = donationDao.findOne(id);
			return donation;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Donation> getDonations() {
		try {
			return donationDao.findAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Donation> getDonationsByPot(int id) {
		try {
			return donationDao.findAllByPot(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Donation> getDonationByUser(int id) {
		try {
			return donationDao.findAllByUser(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void updateDonation(Donation donation) {
		try {
			donationDao.update(donation);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean deleteDonation(int id) {
		try {
			donationDao.delete(id);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

}
