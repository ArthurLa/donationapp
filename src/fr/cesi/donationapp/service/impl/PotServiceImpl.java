package fr.cesi.donationapp.service.impl;

import java.sql.SQLException;
import java.util.List;

import fr.cesi.donationapp.business.Organisation;
import fr.cesi.donationapp.business.Pot;
import fr.cesi.donationapp.dao.PotDao;
import fr.cesi.donationapp.dao.impl.PotDaoImpl;
import fr.cesi.donationapp.service.PotService;

public class PotServiceImpl implements PotService {
	private PotDao potDao;
	
	public PotServiceImpl() {
		potDao = new PotDaoImpl();
	}

	@Override
	public Pot addPot(String name, String description, float amountRequested, Organisation owner, String picture) {
		Pot pot = new Pot(name, description, amountRequested, owner, picture);
		try {
			pot = potDao.create(pot);
			return pot;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Pot getPotById(int id) {
		Pot pot = null;
		try {
			pot = potDao.findOne(id);
			return pot;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Pot> getPots() {
		try {
			return potDao.findAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Pot> getPotsByOrganisation(int id) {
		try {
			return potDao.findAllByOrganisation(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Pot> getPotsByUser(int id) {
		try {
			return potDao.findAllByUser(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void updatePot(Pot pot) {
		try {
			potDao.update(pot);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean deletePot(int id) {
		try {
			potDao.delete(id);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

}
