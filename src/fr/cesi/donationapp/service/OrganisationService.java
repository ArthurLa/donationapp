package fr.cesi.donationapp.service;

import java.util.List;

import fr.cesi.donationapp.business.Organisation;
import fr.cesi.donationapp.business.User;

public interface OrganisationService {
	//c
	public Organisation addOrganisation(String name, String description, String siret, String picture, User user);
		
	//r
	public Organisation getOrganisationById(int id);
	public List<Organisation> getOrganisations();
	public List<Organisation> getOrganisationsByUser(int id);
		
	//u	
	public void updateOrganisation(Organisation organisation);
		
	//d
	public boolean deleteOrganisation(int id);
}
