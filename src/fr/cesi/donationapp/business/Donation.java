package fr.cesi.donationapp.business;

import java.util.Date;

public class Donation {
	private int id;
	private float amount;
	private User recipient;
	private Pot pot;
	private Date date;

	public Donation() {
		date = new Date();
	}
	public Donation(float amount, User recipient, Pot pot) {
		this();
		this.recipient = recipient;
		this.amount = amount;
		this.pot = pot;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public User getRecipient() {
		return recipient;
	}
	public void setRecipient(User recipient) {
		this.recipient = recipient;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Pot getPot() {
		return pot;
	}
	public void setPot(Pot pot) {
		this.pot = pot;
	}
	
	@Override
	public String toString() {
		return "Donation [id=" + id + ", amount=" + amount + ", recipient=" + recipient + ", pot=" + pot + ", date="
				+ date + "]";
	}
}
