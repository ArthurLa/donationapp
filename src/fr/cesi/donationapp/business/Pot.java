package fr.cesi.donationapp.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pot {
	private int id;
	private String title;
	private String description;
	private float amountRequested;
	private Organisation owner;
	private String picture;
	private Date date;
	private List<Donation> donations;

	public Pot() {
		date = new Date();
		donations = new ArrayList<>();
	}

	public Pot(String title, String description, float amountRequested, Organisation owner, String picture) {
		this();
		this.title = title;
		this.description = description;
		this.amountRequested = amountRequested;
		this.owner = owner;
		this.picture = picture;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getAmountRequested() {
		return amountRequested;
	}

	public void setAmountRequested(float amountRequested) {
		this.amountRequested = amountRequested;
	}

	public Organisation getOwner() {
		return owner;
	}

	public void setOwner(Organisation owner) {
		this.owner = owner;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Donation> getDonations() {
		return donations;
	}

	public void setDonations(List<Donation> donations) {
		this.donations = donations;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public float getAmountObtained() {
		float amount = 0;
		for (final Donation donation : this.donations) {
			amount = amount + donation.getAmount();
		}
		return amount;
	}

	@Override
	public String toString() {
		return "Pot [id=" + id + ", title=" + title + ", description=" + description + ", amountRequested="
				+ amountRequested + ", owner=" + owner + ", picture=" + picture + ", date=" + date + ", donations="
				+ donations + "]";
	}
}
