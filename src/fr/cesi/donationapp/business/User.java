package fr.cesi.donationapp.business;

import java.util.ArrayList;
import java.util.List;

public class User {
	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private String pseudo;
	private String password;
	private boolean administrator;
	private List<Donation> donations;
	
	public User() {
		donations = new ArrayList<>();
	}
	public User(String email, String password) {
		this();
		this.email = email;
		this.password = password;
	}
	public User(String pseudo, String firstName, String lastName, String email, String password, boolean administrator) {
		this.pseudo = pseudo;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.administrator = administrator;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isAdmin() {
		return administrator;
	}
	public void setAdmin(boolean administrator) {
		this.administrator = administrator;
	}
	public List<Donation> getDonations() {
		return donations;
	}
	public void setDonations(List<Donation> donations) {
		this.donations = donations;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", pseudo=" + pseudo + ", password=" + password + ", administrator=" + administrator + ", donations="
				+ donations + "]";
	}
}
