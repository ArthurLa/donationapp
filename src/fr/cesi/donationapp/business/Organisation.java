package fr.cesi.donationapp.business;

import java.util.ArrayList;
import java.util.List;

public class Organisation {
	private int id;
	private String name;
	private String description;
	private String siret;
	private String picture;
	private User manager;
	private List<Pot> pots;
	
	public Organisation() {
		pots = new ArrayList<>();
	}
	public Organisation(String name, String description, String siret, String picture, User manager) {
		this();
		this.name = name;
		this.description = description;
		this.siret = siret;
		this.picture = picture;
		this.manager = manager;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public User getManager() {
		return manager;
	}
	public void setManager(User manager) {
		this.manager = manager;
	}
	public List<Pot> getPots() {
		return pots;
	}
	public void setPots(List<Pot> pots) {
		this.pots = pots;
	}
	
	@Override
	public String toString() {
		return "Organisation [id=" + id + ", name=" + name + ", description=" + description + ", siret=" + siret
				+ ", manager=" + manager + ", pots=" + pots + "]";
	}
}
