<%@attribute name="sectionTitle" fragment="true" %>
<%@attribute name="sectionBody" fragment="true" %>

<div class="row">
	<div class="col-lg-2 col-md-3 col-sm-4 col-12"></div>
	<div class="col-lg-10 col-md-9 col-sm-8 col-12">
		<div class="row align-items-center">
			<div class="col-lg-3 col-md-4 col-sm-12 col-12 py-3 mt-3">
				<img id="ProfilePicture" class="d-block mx-auto" src="https://www.belin.re/wp-content/uploads/2018/11/default-avatar.png">
			</div>
			<div class="col-lg-9 col-md-8 col-sm-12 col-12 py-3 mt-3">
				<h1 class="h2">${user.getFirstName()} ${user.getLastName()}</h1>
				<p class="h4">${user.getEmail()}</p>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-2 col-md-3 col-sm-4 col-12 pt-3">
		<div class="d-grid gap-2 mx-auto">
			<a class="btn btn-light" href="${pageContext.request.contextPath}/user/AccountServlet">Mon compte</a>
			<a class="btn btn-light" href="${pageContext.request.contextPath}/user/DonationServlet">Mes dons</a>
			<a class="btn btn-light" href="${pageContext.request.contextPath}/user/OrganisationServlet">Mes associations</a>
			<a class="btn btn-light" href="${pageContext.request.contextPath}/user/PotsServlet">Mes cagnottes</a>
			<a class="btn btn-light" href="${pageContext.request.contextPath}/LogoutServlet">Deconnexion</a>
		</div>
	</div>
	<div class="col-lg-10 col-md-9 col-sm-8 col-12">
		<div class="row align-items-center">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12 bg-white px-5 py-3 mt-3">
				<jsp:invoke fragment="sectionBody"/>
			</div>
		</div>
	</div>
</div>