<%@tag pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><jsp:invoke fragment="title"/></title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
</head>
<body style="min-height: 100vh; background-color: #f1efeb;">
<nav class="navbar navbar-expand-lg navbar-light bg-white">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">BestDeed</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="${pageContext.request.contextPath}/IndexServlet">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/OrganisationServlet">Les associations</a>
        </li>
      </ul>
      <c:if test="${sessionScope.user == null}">
      <a href="${pageContext.request.contextPath}/LoginServlet" class="btn btn-primary">Connexion</a>
      </c:if>
      <c:if test="${sessionScope.user != null}">
      <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
      	<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            ${user.getFirstName()} ${user.getLastName()}
          </a>
          <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="${pageContext.request.contextPath}/user/AccountServlet">Mon compte</a></li>
            <li><a class="dropdown-item" href="${pageContext.request.contextPath}/user/DonationServlet">Mes dons</a></li>
			<li><a class="dropdown-item" href="${pageContext.request.contextPath}/user/DonationServlet">Mes dons</a></li>
			<li><a class="dropdown-item" href="${pageContext.request.contextPath}/user/OrganisationServlet">Mes associations</a></li>
			<li><a class="dropdown-item" href="${pageContext.request.contextPath}/user/PotsServlet">Mes cagnottes</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="${pageContext.request.contextPath}/LogoutServlet">Déconnexion</a></li>
          </ul>
        </li>
      </ul>
      </c:if>
    </div>
  </div>
</nav>
  <div id="pageheader">
    <jsp:invoke fragment="header"/>
 </div>
 <div id="body">
   <jsp:doBody/>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
 </div>
 <div id="pagefooter">
   <jsp:invoke fragment="footer"/>
  </div>
</body>
</html>