-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 17, 2021 at 06:13 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `donation_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE `donation` (
  `id` int(11) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `pot_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`id`, `amount`, `date`, `user_id`, `pot_id`) VALUES
(1, '25', '2021-03-16 21:18:45', 3, 2),
(2, '150', '2021-03-16 21:18:45', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `organisation`
--

CREATE TABLE `organisation` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `siret` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organisation`
--

INSERT INTO `organisation` (`id`, `name`, `description`, `siret`, `user_id`) VALUES
(1, 'Les petits orphelins', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id volutpat neque, ut ullamcorper eros. Sed at erat at quam consequat consectetur ac nec purus. Ut rhoncus et erat et dictum. Suspendisse sodales neque eu nunc pulvinar finibus. Pellentesque ut massa tempus, feugiat ante ut, viverra odio. Cras ac tincidunt ante. Aenean erat tellus, finibus eu lorem eu, dapibus lacinia urna. Donec hendrerit aliquam neque nec ullamcorper. ', '12465812318', 1),
(2, 'Sea shepherd', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id volutpat neque, ut ullamcorper eros. Sed at erat at quam consequat consectetur ac nec purus. Ut rhoncus et erat et dictum. Suspendisse sodales neque eu nunc pulvinar finibus. Pellentesque ut massa tempus, feugiat ante ut, viverra odio. Cras ac tincidunt ante. Aenean erat tellus, finibus eu lorem eu, dapibus lacinia urna. Donec hendrerit aliquam neque nec ullamcorper. ', '498848489499', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pot`
--

CREATE TABLE `pot` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `amount` decimal(10,0) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pot`
--

INSERT INTO `pot` (`id`, `title`, `description`, `amount`, `organisation_id`, `picture`) VALUES
(1, 'Des jouets pour le nouvel orphelina', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id volutpat neque, ut ullamcorper eros. Sed at erat at quam consequat consectetur ac nec purus. Ut rhoncus et erat et dictum. Suspendisse sodales neque eu nunc pulvinar finibus. Pellentesque ut massa tempus, feugiat ante ut, viverra odio. Cras ac tincidunt ante. Aenean erat tellus, finibus eu lorem eu, dapibus lacinia urna. Donec hendrerit aliquam neque nec ullamcorper. ', '2000', 1, 'img/jeux-enfants.jpg'),
(2, 'Soutenez-nous dans nos actions de préservation des dauphins', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id volutpat neque, ut ullamcorper eros. Sed at erat at quam consequat consectetur ac nec purus. Ut rhoncus et erat et dictum. Suspendisse sodales neque eu nunc pulvinar finibus. Pellentesque ut massa tempus, feugiat ante ut, viverra odio. Cras ac tincidunt ante. Aenean erat tellus, finibus eu lorem eu, dapibus lacinia urna. Donec hendrerit aliquam neque nec ullamcorper. ', '150000', 2, 'img/dauphins-mort.jpg'),
(3, 'Luttons contre la pêche à la baleine', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id volutpat neque, ut ullamcorper eros. Sed at erat at quam consequat consectetur ac nec purus. Ut rhoncus et erat et dictum. Suspendisse sodales neque eu nunc pulvinar finibus. Pellentesque ut massa tempus, feugiat ante ut, viverra odio. Cras ac tincidunt ante. Aenean erat tellus, finibus eu lorem eu, dapibus lacinia urna. Donec hendrerit aliquam neque nec ullamcorper. ', '110000', 2, 'img/baleinier.jpg'),
(4, 'Vacances d\'été pour les orphelins', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id volutpat neque, ut ullamcorper eros. Sed at erat at quam consequat consectetur ac nec purus. Ut rhoncus et erat et dictum. Suspendisse sodales neque eu nunc pulvinar finibus. Pellentesque ut massa tempus, feugiat ante ut, viverra odio. Cras ac tincidunt ante. Aenean erat tellus, finibus eu lorem eu, dapibus lacinia urna. Donec hendrerit aliquam neque nec ullamcorper. ', '6000', 1, 'img/vacances-orphelins.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(100) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `pseudo`, `first_name`, `last_name`, `email`, `password`, `is_admin`) VALUES
(1, 'arth.laveau', 'Arthur', 'Laveau', 'arth.laveau@gmail.com', 'user', 1),
(2, 'jmpv', 'Jean Michel', 'Pauvet', 'jmpv@gmail.com', 'user', 0),
(3, 'thomas.leblanc', 'Thomas', 'Leblanc', 'thomas.leblanc@gmail.com', 'user', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `donation`
--
ALTER TABLE `donation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Donation_User_FK` (`user_id`),
  ADD KEY `Donation_Pot_FK` (`pot_id`);

--
-- Indexes for table `organisation`
--
ALTER TABLE `organisation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Organisation_User_FK` (`user_id`);

--
-- Indexes for table `pot`
--
ALTER TABLE `pot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Pot_Organisation_FK` (`organisation_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `donation`
--
ALTER TABLE `donation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `organisation`
--
ALTER TABLE `organisation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pot`
--
ALTER TABLE `pot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `donation`
--
ALTER TABLE `donation`
  ADD CONSTRAINT `Donation_Pot_FK` FOREIGN KEY (`pot_id`) REFERENCES `pot` (`id`),
  ADD CONSTRAINT `Donation_User_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `organisation`
--
ALTER TABLE `organisation`
  ADD CONSTRAINT `Organisation_User_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `pot`
--
ALTER TABLE `pot`
  ADD CONSTRAINT `Pot_Organisation_FK` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
