<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">Mes cagnottes</jsp:attribute>
	<jsp:body>
        <div class="container mb-4">
        	<t:accountpage>
	        	<jsp:attribute name="sectionBody">
	        		<h2 class="h2">Mes cagnottes <a href="${pageContext.request.contextPath}/user/CreatePotServlet" class="btn btn-primary float-end">Créer une cagnotte</a></h2>
					<div class="divider"></div>
					<c:if test="${pots.size() == 0}">Aucune cagnotte pour le moment</c:if>
	        		<c:forEach items="${pots}" var="pot">
	     				<div class="card mb-3">
							<div class="row g-0">
								<div class="col-md-4">
									<img class="w-100" src="${pageContext.request.contextPath}/img/${pot.getPicture()}">
				    			</div>
				    			<div class="col-md-8">
				      				<div class="card-body">
				        				<h5 class="card-title">${pot.getTitle()}</h5>
				        				<p class="card-text">${fn:substring(pot.getDescription(), 0, 100)}...</p>
				      				</div>
				    			</div>
				  			</div>
						</div>
	      			</c:forEach>
	        	</jsp:attribute>	
        	</t:accountpage>				
		</div>
    </jsp:body>
</t:genericpage>