<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">Accueil</jsp:attribute>
	<jsp:body>
        <div class="container">
        	<div class="row">
        		<div class="col-lg-12 col-md-12 col-sm-12 col-12 bg-white px-5 py-3 mt-3">
        			<h1 class="h2 text-center"> Les associations</h1>
        			<div class="divider mx-auto"></div>
        			<div class="row row-cols-1 row-cols-md-3 g-4">
        			<c:forEach items="${organisations}" var="organisation">
					  <div class="col">
						  <div class="card bg-dark text-white">
							  <img src="${pageContext.request.contextPath}/img/${organisation.getPicture()}" class="card-img">
							  <div class="card-img-overlay">
							    <h5 class="card-title">${organisation.getName()}</h5>
							  </div>
						  </div>
					  </div>
  					</c:forEach>
					</div>
				</div>
			</div>
		</div>
    </jsp:body>
</t:genericpage>