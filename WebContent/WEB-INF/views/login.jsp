<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:attribute name="title">Connexion</jsp:attribute>
    <jsp:body>
        <div class="container">
        	<div class="row">
        		<div class="col-lg-6 col-md-8 col-sm-10 col-12 mx-auto mt-5 p-5 bg-white">
        			<h1 class="h3">Connectez-vous</h1>
					<form action="LoginServlet" method="post">
						<div class="mb-3">
							<label for="EMAIL">Email</label>
							<input type="email" class="form-control" name="EMAIL" placeholder="Entrez votre email">
						</div>
						<div class="mb-3">
							<label for="PASSWORD">Mot de passe</label>
							<input type="password" class="form-control" name="PASSWORD" placeholder="Entrez votre mot de passe">
						</div>
						<button type="submit" class="btn btn-primary">Connexion</button>
					</form>
					<a href="RegistrationServlet">Pas encore inscrit ? Créez un compte</a>
				</div>
			</div>
		</div>
    </jsp:body>
</t:genericpage>