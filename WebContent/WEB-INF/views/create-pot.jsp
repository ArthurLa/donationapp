<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">Créer une cagnotte</jsp:attribute>
	<jsp:body>
        <div class="container mb-4">
        	<t:accountpage>
	        	<jsp:attribute name="sectionBody">
	        		<h2 class="h2">Créer une cagnotte</h2>
					<div class="divider"></div>
	        		<form action="${pageContext.request.contextPath}/user/CreatePotServlet" method="POST" enctype="multipart/form-data">
        				<div class="row mb-3">
					    	<label for="TITLE" class="col-sm-3 col-form-label">Title</label>
					    <div class="col-sm-9">
					    	<input type="text" class="form-control" id="TITLE" name="TITLE">
					    </div>
					  	</div>
					  	<div class="row mb-3">
					  		<label for="DESCRIPTION" class="col-sm-3 col-form-label">Description</label>
						    <div class="col-sm-9">
						    	<input type="text" class="form-control" id="DESCRIPTION" name="DESCRIPTION">
						    </div>
						</div>
					  	<div class="row mb-3">
					    	<label for="AMOUNT" class="col-sm-3 col-form-label">Amount</label>
					    	<div class="col-sm-9">
					      		<input type="number" class="form-control" id="AMOUNT" name="AMOUNT" step="0.01">
					    	</div>
					  	</div>
					  	<div class="row mb-3">
					    	<label for="ORGANISATION" class="col-sm-3 col-form-label">Association</label>
					    	<div class="col-sm-9">
						    	<select class="form-select" aria-label="Association" id="ORGANISATION" name="ORGANISATION">
						    	<c:forEach items="${organisations}" var="organisation">
								  <option value="${organisation.getId()}">${organisation.getName()}</option>
							  	</c:forEach>
								</select>
							</div>
					  	</div>
					  	<div class="row mb-3">
					    	<label for="PICTURE" class="col-sm-3 col-form-label">Photo</label>
					    	<div class="col-sm-9">
					      		<input class="form-control" type="file" id="PICTURE" name="PICTURE">
					    	</div>
					  	</div>
					  	<div class="d-grid gap-2 d-md-flex justify-content-md-end">
					  		<button type="submit" class="btn btn-primary">Créer</button>
					  	</div>
				  	</form>
	        	</jsp:attribute>	
        	</t:accountpage>
		</div>
    </jsp:body>
</t:genericpage>