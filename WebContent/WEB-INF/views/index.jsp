<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">Accueil</jsp:attribute>
	<jsp:body>
        <div class="container">
        	<div class="row">
        		<div class="col-lg-12 col-md-12 col-sm-12 col-12 bg-white px-5 py-3 mt-3">
        			<p class="text-center mb-0 sub-title">EXPRIMEZ VOTRE GÉNÉROSITÉ</p>
        			<h1 class="h2 text-center"> Les cagnottes du moment</h1>
        			<div class="divider mx-auto"></div>
        			<div class="row row-cols-1 row-cols-md-3 g-4">
        			<c:forEach items="${pots}" var="pot">
					  <div class="col">
					    <div class="card h-100">
					      <img src="${pageContext.request.contextPath}/img/${pot.getPicture()}" class="card-img-top" alt="...">
					      <div class="card-body">
					        <h5 class="card-title">${pot.getTitle()}</h5>
					        <p class="card-text">${fn:substring(pot.getDescription(), 0, 150)}...</p>
					        <a href="PotDetailServlet/${pot.getId()}" class="btn btn-light">En savoir plus</a>
					      </div>
					      <div class="card-footer">
					        <small class="text-muted">${pot.getDate()}</small>
					      </div>
					    </div>
					  </div>
  					</c:forEach>
					</div>
				</div>
			</div>
		</div>
    </jsp:body>
</t:genericpage>