<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">Mon compte</jsp:attribute>
	<jsp:body>
        <div class="container mb-4">
        	<t:accountpage>
	        	<jsp:attribute name="sectionBody">
	        		<h2 class="h2">Créer une association</h2>
					<div class="divider"></div>
	        		<form action="${pageContext.request.contextPath}/user/CreateOrganisationServlet" method="POST" enctype="multipart/form-data">
        				<div class="row mb-3">
					    	<label for="NAME" class="col-sm-3 col-form-label">Nom</label>
					    <div class="col-sm-9">
					    	<input type="text" class="form-control" id="NAME" name="NAME">
					    </div>
					  	</div>
					  	<div class="row mb-3">
					  		<label for="DESCRIPTION" class="col-sm-3 col-form-label">Description</label>
						    <div class="col-sm-9">
						    	<input type="text" class="form-control" id="DESCRIPTION" name="DESCRIPTION">
						    </div>
						</div>
					  	<div class="row mb-3">
					    	<label for="SIRET" class="col-sm-3 col-form-label">Siret</label>
					    	<div class="col-sm-9">
					      		<input type="text" class="form-control" id="SIRET" name="SIRET">
					    	</div>
					  	</div>
					  	<div class="row mb-3">
					    	<label for="PICTURE" class="col-sm-3 col-form-label">Photo</label>
					    	<div class="col-sm-9">
					      		<input class="form-control" type="file" id="PICTURE" name="PICTURE">
					    	</div>
					  	</div>
					  	<div class="d-grid gap-2 d-md-flex justify-content-md-end">
					  		<button type="submit" class="btn btn-primary">Créer</button>
					  	</div>
				  	</form>
	        	</jsp:attribute>	
        	</t:accountpage>
		</div>
    </jsp:body>
</t:genericpage>