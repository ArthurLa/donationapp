<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">Mon compte</jsp:attribute>
	<jsp:body>
        <div class="container mb-4">
        	<t:accountpage>
	        	<jsp:attribute name="sectionBody">
	        		<h2 class="h2">Mes associations <a href="${pageContext.request.contextPath}/user/CreateOrganisationServlet" class="btn btn-primary float-end">Créer une association</a></h2>
					<div class="divider"></div>
					<c:if test="${organisations.size() == 0}">Aucune association pour le moment</c:if>
	        		<c:forEach items="${organisations}" var="organisation">
        				<div class="card mb-3">
						  <div class="row g-0">
						    <div class="col-md-4">
						      <img class="w-100" src="${pageContext.request.contextPath}/img/${organisation.getPicture()}">
						    </div>
						    <div class="col-md-8">
						      <div class="card-body">
						        <h5 class="card-title">${organisation.getName()}</h5>
						        <p class="card-text">${fn:substring(organisation.getDescription(), 0, 100)}...</p>
						      </div>
						    </div>
						  </div>
						</div>
		        	</c:forEach>
	        	</jsp:attribute>	
        	</t:accountpage>
		</div>
    </jsp:body>
</t:genericpage>