<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">Mon compte</jsp:attribute>
	<jsp:body>
        <div class="container mb-4">
			<t:accountpage>
	        	<jsp:attribute name="sectionBody">
	        		<div class="row">
	        			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
		        			<h2 class="h2">Informations</h2>
							<div class="divider"></div>
		        			<form action="${pageContext.request.contextPath}/user/AccountServlet" method="POST">
							  <div class="row mb-3">
							    <label for="FIRST_NAME" class="col-sm-3 col-form-label">Prénom</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="FIRST_NAME" name="FIRST_NAME" value="${user.getFirstName()}">
							    </div>
							  </div>
							  <div class="row mb-3">
							    <label for="LAST_NAME" class="col-sm-3 col-form-label">Nom</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="LAST_NAME" name="LAST_NAME" value="${user.getLastName()}">
							    </div>
							  </div>
							  <div class="row mb-3">
							    <label for="EMAIL" class="col-sm-3 col-form-label">Email</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="EMAIL" name="EMAIL" value="${user.getEmail()}">
							    </div>
							  </div>
							  <div class="d-grid gap-2 d-md-flex justify-content-md-end">
							  	<button type="submit" class="btn btn-primary">Mettre à jour</button>
							  </div>
						  </form>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-12">
		        			<h2 class="h2">Mot de passe</h2>
		        			<div class="divider"></div>
		        			<form action="${pageContext.request.contextPath}/user/AccountServlet" method="POST">
							  <div class="row mb-3">
							    <label for="OLD_PASSWORD" class="col-sm-3 col-form-label">Ancien mot de passe</label>
							    <div class="col-sm-9">
							      <input type="password" class="form-control" id="OLD_PASSWORD" name="OLD_PASSWORD">
							    </div>
							  </div>
							  <div class="row mb-3">
							    <label for="NEW_PASSWORD" class="col-sm-3 col-form-label">Nouveau mot de passe</label>
							    <div class="col-sm-9">
							      <input type="password" class="form-control" id="NEW_PASSWORD" name="NEW_PASSWORD">
							    </div>
							  </div>
							  <div class="row mb-3">
							    <label for="CONFIRM_PASSWORD" class="col-sm-3 col-form-label">Confirmation</label>
							    <div class="col-sm-9">
							      <input type="password" class="form-control" id="CONFIRM_PASSWORD" name="CONFIRM_PASSWORD">
							    </div>
							  </div>
							  <div class="d-grid gap-2 d-md-flex justify-content-md-end">
							  	<button type="submit" class="btn btn-primary">Mettre à jour</button>
							  </div>
						  </form>
						</div>
	        		</div>
	        	</jsp:attribute>	
        	</t:accountpage>		
		</div>
    </jsp:body>
</t:genericpage>