<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">Mon compte</jsp:attribute>
	<jsp:body>
        <div class="container mb-4">
        	<t:accountpage>
	        	<jsp:attribute name="sectionBody">
	        		<h2 class="h2">Mes dons</h2>
					<div class="divider"></div>
					<c:if test="${donations.size() == 0}">Aucun don pour le moment</c:if>
	        		<c:forEach items="${donations}" var="donation">
        				<div class="card mb-3">
						  <div class="row g-0">
						    <div class="col-md-4">
						      <img class="w-100" src="${pageContext.request.contextPath}/img/${donation.getPot().getPicture()}">
						    </div>
						    <div class="col-md-8">
						      <div class="card-body">
						        <h5 class="card-title">${donation.getPot().getTitle()}</h5>
						        <p class="card-text">${fn:substring(donation.getPot().getDescription(), 0, 100)}...</p>
						        <p class="card-text text-danger">Montant: ${donation.getAmount().toString()} €</p>
						        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
						      </div>
						    </div>
						  </div>
						</div>
        			</c:forEach>
	        	</jsp:attribute>	
        	</t:accountpage>
		</div>
    </jsp:body>
</t:genericpage>