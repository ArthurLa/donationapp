<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:attribute name="title">Créer un compte</jsp:attribute>
    <jsp:body>
        <div class="container">
        	<div class="row">
        		<div class="col-lg-6 col-md-8 col-sm-10 col-12 mx-auto mt-5 p-5 bg-white">
        			<h1 class="h3">Pas encore inscrit ? Créez vous un compte</h1>
					<form action="RegistrationServlet" method="post">
						<div class="mb-3">
							<label for="LAST_NAME">Nom</label>
							<input type="text" name="LAST_NAME" class="form-control" placeholder="Nom">
						</div>
						<div class="mb-3">
							<label for="FIRST_NAME">Prénom</label>
							<input type="text" name="FIRST_NAME" class="form-control" placeholder="Prénom">
						</div>
						<div class="mb-3">
							<label for="EMAIL">Email</label>
							<input type="email" class="form-control" name="EMAIL" placeholder="Entrez votre email">
						</div>
						<div class="mb-3">
							<label for="PASSWORD">Mot de passe</label>
							<input type="password" class="form-control" name="PASSWORD" placeholder="Entrez votre mot de passe">
						</div>
						<button type="submit" class="btn btn-primary">Créer un compte</button>
					</form>
				</div>
			</div>
		</div>
    </jsp:body>
</t:genericpage>