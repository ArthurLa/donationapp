<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="title">${pot.getTitle()}</jsp:attribute>
	<jsp:body>
        <div class="container">
        	<div class="row">
        		<div class="col-lg-9 col-md-8 col-sm-12 col-12">
        			<div class="mt-3">
        				<img src="${pageContext.request.contextPath}/img/${pot.getPicture()}" class="card-img-top" alt="...">
        			</div>
        			<div class="bg-white p-5">
	        			<h1 class="h2">${pot.getTitle()}</h1>
				        <p class="card-text">${pot.getDescription()}</p>
			        </div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-12 col-12">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="bg-white px-5 p-3 mt-3">
								<p id="AmountObtained" class="text-center mb-2">${pot.getAmountObtained().toString()} €</p>
								<p class="text-center">COLLECTÉS SUR ${pot.getAmountRequested().toString()} €</p>
								<p class="text-center"><span class="d-block text-center"><i class="bi bi-people-fill"></i></span> ${pot.getDonations().size().toString()} <c:if test="${pot.getDonations().size() <= 1}">participation</c:if><c:if test="${pot.getDonations().size() > 1}">participations</c:if></p>
								<c:if test="${sessionScope.user != null}">
								<div class="divider mx-auto"></div>
								<form action="${pageContext.request.contextPath}/CreateDonationServlet/${pot.getId()}" method="POST">
									<div class="mb-3">
									   	<label for="AMOUNT" class="form-label">Montant</label>
									    <input type="number" class="form-control" id="AMOUNT" name="AMOUNT" aria-describedby="amountHelp">
									    <div id="amountHelp" class="form-text">Montant que vous souhaitez donner à cette cagnotte.</div>
								 	</div>
									<div class="d-grid gap-2">
										<button class="btn btn-primary d-block">Je participe</button>
									</div>
								</form>
								</c:if>
							</div>
							<div class="bg-white px-5 p-3 mt-3">
								<h4>${pot.getOwner().getName()}</h4>
								<div class="divider"></div>
								<p class="card-text">${fn:substring(pot.getDescription(), 0, 100)}... </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </jsp:body>
</t:genericpage>